# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

#### 1. brush ![](https://i.imgur.com/bDbABVi.jpg)

Click the icon, you can draw on canvas
#### 2. eraser ![](https://i.imgur.com/6DfDmZd.jpg)
Click the icon, you can erase what you have drawn.
#### 3. text ![](https://i.imgur.com/JwJ7k24.jpg)
Click the icon, then click the position on the canvas you want, there will be a text box to type text in the left top. Type texts in the text box, and the texts will show on the canvas on the position that you clicked.
#### 4. circle ![](https://i.imgur.com/LIq69fV.png)
Click the icon, then draw a hollow circle.
#### 5. rectangle ![](https://i.imgur.com/RGz37EG.png)
Click the icon, then draw a hollow rectangle.
#### 6. triangle ![](https://i.imgur.com/njFHkQv.png)
Click the icon, then draw a solid triangle.
#### 7. download ![](https://i.imgur.com/wpbynDN.png)
Click the icon, then it will download the image as "filename.png"
#### 8. upload ![](https://i.imgur.com/wLvRj1U.png)
Click the icon, then you can upload your image to the canvas.
#### 9. undo ![](https://i.imgur.com/RebF0mj.jpg)
Click the icon, then you can go to the former step.
#### 10. redo ![](https://i.imgur.com/fJBZ8Gi.jpg)
Click the icon, then you can go to the backward step after you click the button "undo".
#### 11. refresh ![](https://i.imgur.com/HnEzoOk.jpg)
Click the icon, then you can will get a clear canvas.
#### 12. ![](https://i.imgur.com/7ijrE7s.png)
Click the block in under "Color:", it will invoke a windows to select color.
#### 13. ![](https://i.imgur.com/XvFyNZj.png)
Select the brush width and text size.
#### 14. ![](https://i.imgur.com/zSZN7Hw.png)
Select the font of your text.
#### 15. cursor change
When you select different features, the cursor will change when it moves to the canvas.
### Function description

I have no bonus function QQ

### Gitlab page link

https://107062140.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

Thank you~~
