// When true, moving the mouse draws on the canvas
var undoArray = new Array();
var redoArray = new Array();
var step = 0;
var bstep;
var color = "#0000ff";
var textbox;
var type;
var mousePressed = false;
var x = 0;
var y = 0;
var myPics;
var context;
var rect;
var textbox;
var store;
var lastX, lastY;
var cStep = -1;
var cPushArray = new Array();

window.addEventListener("load", function(event) {
    // All resources finished loading!
    myPics = document.getElementById('myCanvas');
    context = myPics.getContext('2d');
    // The x and y offset of the canvas from the edge of the page
    rect = myPics.getBoundingClientRect();
    textbox = document.getElementById('textbox');
    textbox.style.visibility = 'hidden';
    context.getImageData(0, 0, myCanvas.width, myCanvas.height);
    cPush();
    
    document.getElementById("brush").onclick = function(){ 
        type = "brush";
        console.log(type);
    };

    document.getElementById("eraser").onclick = function(){
        type = "eraser";
        console.log(type);
    };

    document.getElementById("text").onclick=function(){ 
        type = "text";
    }

    document.getElementById("circle").onclick=function(){ 
        type = "circle";
    }

    document.getElementById("rectangle").onclick=function(){ 
        type = "rectangle";
    }

    document.getElementById("triangle").onclick=function(){ 
        type = "triangle";
    }

    document.getElementById("download").onclick=function(){ 
        console.log("download");
        var link = document.createElement('a');
        link.download = 'filename.png';
        link.href = document.getElementById('myCanvas').toDataURL()
        link.click();
        textbox.style.visibility = 'hidden';
    }

    document.getElementById("upload").onchange=function(){ 
        var img = new Image();
        img.onload = draw;
        img.onerror = failed;
        img.src = URL.createObjectURL(this.files[0]);
        textbox.style.visibility = 'hidden';
    }

    document.getElementById("undo").onclick=function(){
        if (cStep > 0) {
            cStep--;
            console.log(cStep);
            var canvasPic;
            canvasPic = cPushArray[cStep];
            context.putImageData(canvasPic, 0, 0); 
            textbox.style.visibility = 'hidden';
            /*document.title = cStep + ":" + cPushArray.length;*/
        }
    }

    document.getElementById("redo").onclick=function(){
        if (cStep < cPushArray.length-1) {
            cStep++;
            var canvasPic;
            canvasPic = cPushArray[cStep];
            context.putImageData(canvasPic, 0, 0);
        }
        textbox.style.visibility = 'hidden';
    }

    document.getElementById("refresh").onclick=function(){
        textbox.style.visibility = 'hidden';
        context.clearRect(0, 0, myPics.width, myPics.height);
        cPush();
        textbox.style.visibility = 'hidden';
    }
    

    myPics.addEventListener("mousedown", function(e){
        if(type == "brush" || type == "eraser"){
            x = e.clientX - rect.left;
            y = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
        }
        else if(type == "circle"){
            store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
            context.globalCompositeOperation='source-over';
        }
        else if(type == "rectangle"){
            console.log("mousedown");
            store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
            context.globalCompositeOperation='source-over';
        }
        else if(type == "triangle"){
            console.log("mousedown");
            store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            mousePressed = true;
            textbox.style.visibility = 'hidden';
            context.globalCompositeOperation='source-over';
        }
        else if(type == "text"){
            console.log("mousedown");
            originalx = e.clientX - rect.left;
            originaly = e.clientY - rect.top;
            store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            textbox.style.visibility = 'visible';

            textbox.onkeydown = function(e) {
                context.putImageData(store, 0, 0);
                context.globalCompositeOperation='source-over';
                context.font = 5*document.getElementById("selWidth").value + "px " + document.getElementById("selfont").value;
                context.fillStyle = document.getElementById("colorWell").value.toString();;
                context.fillText(textbox.value, originalx, originaly);
            }
        }
    });

    myPics.addEventListener("mousemove", function(e){
        if(type == "brush"){
            if (mousePressed) {
                Draw(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
                //console.log(flag+" in brush");
                x = e.clientX - rect.left;
                y = e.clientY - rect.top;
            }
            $("canvas#myCanvas").css('cursor', 'url(brushs.png), auto'); 
        }
        else if(type == "eraser"){
            if (mousePressed) {
                _Draw(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
                //console.log(flag+" in brush");
                x = e.clientX - rect.left;
                y = e.clientY - rect.top;
            }
            $("canvas#myCanvas").css('cursor', 'url(erasers.png), auto'); 
        }
        else if(type == "text"){
            $("canvas#myCanvas").css('cursor', 'text'); 
        }
        else if(type == "circle"){
            if (mousePressed === true) {
                context.putImageData(store, 0, 0);
                console.log(store);
                drawcircle(context, originalx, originaly, e.clientX - rect.left, e.clientY - rect.top);
            }
            $("canvas#myCanvas").css('cursor', 'url(circle.png), auto'); 
        }
        else if(type == "rectangle"){
            if (mousePressed === true) {
                console.log("mousemove");
                context.putImageData(store, 0, 0);
                drawrectangle(context, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
            }
            $("canvas#myCanvas").css('cursor', 'url(rectangles.png), auto'); 
        }
        else if(type == "triangle"){
            if (mousePressed === true) {
                console.log("mousemove");
                context.putImageData(store, 0, 0);
                drawtriangle(context, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
            }
            $("canvas#myCanvas").css('cursor', 'url(triangles.png), auto'); 
        };
    });

    myPics.addEventListener("mouseup", function(e){
        if(type == "brush"){
            if(mousePressed){
                Draw(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
                x = 0;
                y = 0;
                mousePressed = false;
            }
        }
        else if(type == "eraser"){
            if(mousePressed){
                _Draw(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
                x = 0;
                y = 0;
                mousePressed = false;
            }
        }
        else if(type == "circle"){
            if (mousePressed === true){
                console.log("mouseup");
                drawcircle(context, originalx, originaly, e.clientX - rect.left, e.clientY - rect.top);
                originalx = 0;
                originaly = 0;
                mousePressed = false;
                store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            }
        }
        else if(type == "rectangle"){
            if (mousePressed === true) {
                console.log("mouseup");
                drawrectangle(context, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
                originalx = 0;
                originaly = 0;
                mousePressed = false;
                store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            }
        }
        else if(type == "triangle"){
            if (mousePressed === true) {
                console.log("mouseup");
                drawtriangle(context, originalx, originaly, e.clientX-rect.left, e.clientY-rect.top);
                originalx = 0;
                originaly = 0;
                mousePressed = false;
                store=context.getImageData(0, 0, myCanvas.width, myCanvas.height);
            }
        }
        cPush();
    });
});

function Draw(context, x1, y1, x2, y2) {
    context.beginPath();
    console.log(color);
    context.strokeStyle = document.getElementById("colorWell").value.toString();
    context.globalCompositeOperation='source-over';
    context.lineWidth = $('#selWidth').val();

    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}


function _Draw(context, x1, y1, x2, y2) {
    context.beginPath();
    context.globalCompositeOperation='destination-out';
    context.lineWidth = $('#selWidth').val();
    
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}

function drawcircle(context, x1, y1, x2, y2) {
    context.beginPath();
    context.strokeStyle = document.getElementById("colorWell").value.toString();;
    context.lineWidth = $('#selWidth').val();;
    context.arc(x1, y1, Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)), 0, Math.PI * 2, true);
    context.stroke();
    context.closePath();
}

function drawrectangle(context, x1, y1, x2, y2) {
    context.beginPath();
    context.strokeStyle = document.getElementById("colorWell").value.toString();;
    context.lineWidth = $('#selWidth').val();;
    context.rect(x1, y1, x2-x1, y2-y1);
    context.stroke();
    
}

function drawtriangle(context, x1, y1, x2, y2) {
    context.beginPath();
    console.log(color);
    context.lineWidth = 1;
    context.moveTo(x1,y1);
    context.lineTo(x2,y2);
    context.lineTo(x1,y2);
    context.fillStyle = document.getElementById("colorWell").value.toString();;
    context.fill();
}

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { 
        cPushArray.length = cStep; 
    }
    cPushArray.push(context.getImageData(0, 0, myPics.width, myPics.height));
}
function draw() {
    myPics.width = this.width;
    myPics.height = this.height;
    context.drawImage(this, 0,0);
    cPush();
}
function failed() {
    console.error("The provided file couldn't be loaded as an Image media");
}